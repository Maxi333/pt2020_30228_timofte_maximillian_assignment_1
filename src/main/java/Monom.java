public class Monom {
    private int exp=0;
    private long coef=0;

    public Monom() {
    }
    public Monom(Long coef, int exp) {
        this.coef=coef;
        this.exp=exp;
    }
    public Monom(Monom mon) {
        exp=mon.getExp();
        coef=mon.getCoef();
    }

    public static Monom adunare2Monomi(Monom p1, Monom p2){
        long x=p1.getCoef()+p2.getCoef();
        p1.setCoef(x);
        x=p2.getExp();
        if(x!=p1.getExp())
            return new Monom((long) 0,(int)x);
        return p1;
    }
    public static Monom scadere2Monomi(Monom p1, Monom p2){
        long x=p1.getCoef()-p2.getCoef();
        p1.setCoef(x);
        if(x!=p1.getExp())
            return new Monom((long) 0,(int)x);
        return p1;
    }
    public static long inmultire2Monomi(Monom p1, Monom p2){
        return p2.getCoef()*p1.getCoef();
    }
    public static Monom impartire2Monomi(Monom p1, Monom p2){//in lucru
        /////////////////////////nu merge a nu se incerca
        //de ce mvc de ce e polinom analiza problemei cum sa fac regex ce e polinom monom operatiile ce se fac use caseurile explicate
        //scenarii docx in link cum fac :tot cu utilizatorul
        //cu diagrama de clase explic ce fa fiecare clasa
        //despre interfata sa scriu cum ai testat
        /*Monom[] coef=new Monom[52];
        double d;
        for(int i=0;i<52;i++) {
            d=((double) (p1.getCoef())[i -1] / (double) (i));
            coef[i] = (p1.getCoef())[i] / (p2.getCoef())[i];

        }
        this.coef=coef;*/return p1;
    }
    public static Monom derivareMonom(Monom p1){
        long x = p1.getCoef()* p1.getExp();
        p1.setCoef(x);
        p1.setExp (p1.getExp()-1);
        return p1;
    }
    public static Monom integrareMonom(Monom p1){
        long x = p1.getExp()+1;
        p1.setExp ((int)x);
        if(x!=0)
            x = p1.getCoef() / (x-1);
        p1.setCoef(x);
        return p1;
    }


    public String compareTo(){
        return String.valueOf(this.getExp());
    }
    public String toString() {
        String a = new String();
        int i=0;
        a = "" + coef + " ";
        return a;
    }


    public int getExp() {
        return exp;
    }
    public void setExp(int exp) {
        this.exp = exp;
    }
    public long getCoef() {
        return coef;
    }
    public void setCoef(long coef) {
        this.coef = coef;
    }
}//de facut getterele si setterele
