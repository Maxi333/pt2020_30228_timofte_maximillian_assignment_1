import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controler {

    private View view;
    private JButton citirePolinom1Button;
    private JButton citirePolinom2Button;
    private JButton adunare2PolinomiButton;
    private JButton scadere2PolinomiButton;
    private JButton inmultire2PolinomiButton;
    private JButton impartire2PolinomiButton;
    private JButton derivarePolinomButton;
    private JButton integrarePolinomButton;
    private JRadioButton iOperatorIs1stRadioButton;
    private JRadioButton iOperatorIs2ndRadioButton;
    private JTextArea textArea1;
    private Polinom p1;//= new Polinom() la initComponents
    private Polinom p2;//= new Polinom() la initComponents
    private Polinom p3;//= new Polinom() la initComponents

    public Controler() {

        initComponents();
        initListeners();
    }

    public void showMainFrame(){
        view.setVisible(true);
    }

    private void initComponents() {
        this.view =new View();
        this.citirePolinom1Button = view.getCitirePolinom1Button();
        this.citirePolinom2Button = view.getCitirePolinom2Button();
        this.adunare2PolinomiButton = view.getAdunare2PolinomiButton();
        this.scadere2PolinomiButton = view.getScadere2PolinomiButton();
        this.inmultire2PolinomiButton = view.getInmultire2PolinomiButton();
        this.impartire2PolinomiButton = view.getImpartire2PolinomiButton();
        this.derivarePolinomButton = view.getDerivarePolinomButton();
        this.integrarePolinomButton = view.getIntegrarePolinomButton();
        this.iOperatorIs1stRadioButton = view.getiOperatorIs1stRadioButton();
        this.iOperatorIs2ndRadioButton = view.getiOperatorIs2ndRadioButton();
        this.textArea1= view.getTextArea1();//initComponents!!!
        p1=new Polinom();
        p2=new Polinom();
        p3=new Polinom();
    }

    private void initListeners() {
        citirePolinom1Button.addActionListener(new citire1ActionListener());
        citirePolinom2Button.addActionListener(new citire2ActionListener());
        adunare2PolinomiButton.addActionListener(new adunareActionListener());
        scadere2PolinomiButton.addActionListener(new scadereActionListener());
        inmultire2PolinomiButton.addActionListener(new inmultireActionListener());
        impartire2PolinomiButton.addActionListener(new impartireActionListener());
        derivarePolinomButton.addActionListener(new derivareActionListener());
        integrarePolinomButton.addActionListener(new integrareActionListener());
        //initializing action listeners for clicking basically
    }

    private class citire1ActionListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            p1.citirePolinom(textArea1.getText());
            textArea1.append("S-a efectuat citirea polinomului!\n");
        }
    }
    private class citire2ActionListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            p2.citirePolinom(textArea1.getText());
            textArea1.append("S-a efectuat citirea polinomului!\n");
        }
    }
    private class adunareActionListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            p3.adunare2Polinomi(p1, p2);
            textArea1.append(p3.toString()+"\n");
        }
    }

    private class scadereActionListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            p3.scadere2Polinomi(p1, p2);
            textArea1.append(p3.toString()+"\n");
        }
    }

    private class inmultireActionListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            p3.inmultire2Polinomi(p1, p2);
            textArea1.append(p3.toString()+"\n");
        }
    }

    private class impartireActionListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            p3.impartire2Polinomi(p1, p2);
            textArea1.append(p3.toString()+"\n");
        }
    }

    private class derivareActionListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            p3.derivarePolinom(p1);
            textArea1.append(p3.toString()+"\n");
        }
    }

    private class integrareActionListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            p3.integrarePolinom(p1);
            textArea1.append(p3.toString()+"\n");
        }
    }

    public Polinom getP1() {
        return p1;
    }

    public void setP1(Polinom p1) {
        this.p1 = p1;
    }

    public Polinom getP2() {
        return p2;
    }

    public void setP2(Polinom p2) {
        this.p2 = p2;
    }

    public Polinom getP3() {
        return p3;
    }

    public void setP3(Polinom p3) {
        this.p3 = p3;
    }
}
