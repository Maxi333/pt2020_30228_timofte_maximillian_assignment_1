import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.ArrayList;
import java.util.Collections;  // Import the Collections class

public class Polinom {
    private ArrayList<Monom> coef=new ArrayList<Monom>();//sir[0] detine lungimea sirului de coeficienti

    public Polinom() {
        for(int i=0;i<58;i++){
            this.coef.add(new Monom((long)0,i));
        }
    }
    public Polinom(Monom coef) {
        for(int i=0;i<58;i++){
            this.coef.add(new Monom(coef.getCoef(),i));
        }
    }
    public ArrayList<Monom> getCoef() {
        return coef;
    }
    public void setCoef(ArrayList<Monom> coef) {
        this.coef = coef;
    }

    public void adunare2Polinomi(Polinom p1, Polinom p2){
        for(int i=0;i<58;i++)
            this.coef.set(i,Monom.adunare2Monomi(p1.getCoef().get(i),p2.getCoef().get(i)));
    }
    public void scadere2Polinomi(Polinom p1, Polinom p2){
        for(int i=0;i<58;i++)
            this.coef.set(i,Monom.scadere2Monomi(p1.getCoef().get(i),p2.getCoef().get(i)));
    }
    public void inmultire2Polinomi(Polinom p1, Polinom p2){
        Monom temp=new Monom();long x;
        for(int i=0; i<58; i++) {///inmultirea fiecare cu fiecare
            //Monom j :p.getCoef()
            x = 0;
            for(int j=0; j<=i/2; j++)//Monom j :p2.getCoef()
                x += Monom.inmultire2Monomi(p1.getCoef().get(i-j), p2.getCoef().get(j));
            temp.setCoef(x);
            temp.setExp(i);
            coef.set (i, new Monom(temp));
        }
    }
    public void impartire2Polinomi(Polinom p1, Polinom p2){//in lucru
        /////////////////////////nu merge a nu se incerca
        /*Monom coef=new Monom();
        double d;
        for(int i=0;i<58;i++) {
            d=((double) (p1.getCoef())[i -1] / (double) (i));
            coef[i] = (p1.getCoef())[i] / (p2.getCoef())[i];

        }
        this.coef=coef;*/
    }
    public void derivarePolinom(Polinom p1){
        for(int i=1;i<58;i++)
            this.coef.set(i-1,Monom.derivareMonom(p1.getCoef().get(i)));
        coef.set(57,new Monom((long)0,57));
    }
    public void integrarePolinom(Polinom p1){
        for(int i=56;i>0;i--)
            this.coef.set(i+1,Monom.integrareMonom(p1.getCoef().get(i)));
        this.coef.set(0,Monom.integrareMonom(p1.getCoef().get(56)));
        coef.set(1, new Monom((long)0,1));
    }
    @Override
    public String toString() {
        String a = new String();
        for (Monom x : coef){
            a = a + x + " ";
    }
        return a;
    }

    public void citirePolinom(String line) {
        // String to be scanned to find the pattern.
        String line1;
        String pattern1 = "(.*)(\\d+)(.*)";
        String coef=new String(" ");
        String grad=new String(" ");
        //line = "Sa se citeasca urmatorul polinom ---------DE CITIT-----: - 43412526*X   ^ 4 + 5241325 *X     ^  1 +         2375732*     X^0";
        //////--------------------------------------------------// - 16*X   ^ 4 + 5 *X     ^  1 +         2*     X^0
        String pattern = "[-|+]\\s*\\d+\\p{javaWhitespace}*[*]*\\p{javaWhitespace}*[X]*\\p{javaWhitespace}*[\\^]+\\p{javaWhitespace}*\\d+";
        String pattern2= "[+-]\\p{javaWhitespace}*[\\d]*";
        String pattern3= "[\\d]+";

        int x=0;
        this.coef.set(0,new Monom());

        // Create a Pattern object
        Pattern r = Pattern.compile(pattern);
        Pattern r2= Pattern.compile(pattern2);
        Pattern r3= Pattern.compile(pattern3);

        // Now create matcher object.
        Matcher m = r.matcher(line);//contine secventele?
        Matcher n;
        Matcher p;
        while (m.find( )) {
            System.out.println("Found value: " + m.group(0) );
            line1 = m.group(0);
            n = r2.matcher (line1);
            if (n.find())
                coef = n.group(0);
            else {
                System.out.println("eroare!!!!!!!!");
            }

            p = r3.matcher(line1);
            //System.out.println("Found value: "+line1);
            while(p.find())
                grad = p.group(0);

            //System.out.println("Found value: "+grad);
            x = Integer.parseInt(grad);
            coef = coef.replaceAll("\\p{javaWhitespace}*","");
            this.coef.set(x,new Monom(Long.parseLong(coef),x));
            //System.out.println("Found value: " + sir[x]);
        }
        if(this.coef.get(0) == new Monom(Long.parseLong(coef),x))
            System.out.println("NO MATCH");
    }
}
