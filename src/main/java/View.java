import javax.swing.*;
import java.awt.*;

public class View extends JFrame{


    private JPanel mainPanel;
    private JButton citirePolinom1Button;
    private JButton citirePolinom2Button;
    private JButton adunare2PolinomiButton;
    private JButton scadere2PolinomiButton;
    private JButton inmultire2PolinomiButton;
    private JButton impartire2PolinomiButton;
    private JButton derivarePolinomButton;
    private JButton integrarePolinomButton;
    private JRadioButton iOperatorIs1stRadioButton;
    private JRadioButton iOperatorIs2ndRadioButton;
    private JTextArea textArea1;



    public View() {
        setSize(1000,1000);
        setContentPane(mainPanel);
        setLocationRelativeTo(null);
    }

    public JButton getCitirePolinom1Button() {
        return citirePolinom1Button;
    }

    public void setCitirePolinom1Button(JButton citirePolinom1Button) {
        this.citirePolinom1Button = citirePolinom1Button;
    }

    public JButton getCitirePolinom2Button() {
        return citirePolinom2Button;
    }

    public void setCitirePolinom2Button(JButton citirePolinom2Button) {
        this.citirePolinom2Button = citirePolinom2Button;
    }

    public JButton getAdunare2PolinomiButton() {
        return adunare2PolinomiButton;
    }

    public void setAdunare2PolinomiButton(JButton adunare2PolinomiButton) {
        this.adunare2PolinomiButton = adunare2PolinomiButton;
    }

    public JButton getScadere2PolinomiButton() {
        return scadere2PolinomiButton;
    }

    public void setScadere2PolinomiButton(JButton scadere2PolinomiButton) {
        this.scadere2PolinomiButton = scadere2PolinomiButton;
    }

    public JButton getInmultire2PolinomiButton() {
        return inmultire2PolinomiButton;
    }

    public void setInmultire2PolinomiButton(JButton inmultire2PolinomiButton) {
        this.inmultire2PolinomiButton = inmultire2PolinomiButton;
    }

    public JButton getImpartire2PolinomiButton() {
        return impartire2PolinomiButton;
    }

    public void setImpartire2PolinomiButton(JButton impartire2PolinomiButton) {
        this.impartire2PolinomiButton = impartire2PolinomiButton;
    }

    public JButton getDerivarePolinomButton() {
        return derivarePolinomButton;
    }

    public void setDerivarePolinomButton(JButton derivarePolinomButton) {
        this.derivarePolinomButton = derivarePolinomButton;
    }

    public JButton getIntegrarePolinomButton() {
        return integrarePolinomButton;
    }

    public void setIntegrarePolinomButton(JButton integrarePolinomButton) {
        this.integrarePolinomButton = integrarePolinomButton;
    }

    public JRadioButton getiOperatorIs1stRadioButton() {
        return iOperatorIs1stRadioButton;
    }

    public void setiOperatorIs1stRadioButton(JRadioButton iOperatorIs1stRadioButton) {
        this.iOperatorIs1stRadioButton = iOperatorIs1stRadioButton;
    }

    public JRadioButton getiOperatorIs2ndRadioButton() {
        return iOperatorIs2ndRadioButton;
    }

    public void setiOperatorIs2ndRadioButton(JRadioButton iOperatorIs2ndRadioButton) {
        this.iOperatorIs2ndRadioButton = iOperatorIs2ndRadioButton;
    }

    public JTextArea getTextArea1() {
        return textArea1;
    }

    public void setTextArea1(JTextArea textArea1) {
        this.textArea1 = textArea1;
    }


}
